#!/usr/bin/env ruby
#Validador de base de luchas sociales
#Vladimir Támara Patiño. vtamara@pasosdeJesus.org. 2020.
#Basado en marco conceptual, documento de jerarquías y MUCHA experimentación
#con los datos

require 'csv'
require 'byebug'

if ARGV[0].nil?
  puts "Falta primer parametro con nombre de archivo por verificar"
  exit 1
end
nomarc = ARGV[0]
if ARGV[1].nil?
  puts "Falta segundo parametro con nombre de archivo de errores"
  exit 1
end
nomerr = ARGV[1]
if ARGV[2].nil?
  puts "Falta tercer parametro con nombre de archivo de advertencias"
  exit 1
end
nomadv = ARGV[2]


# Rationale para usar CSV

# Es dificil la lectua de XLSX con tantos datos. Sólo con 2019:
# * roo falló por memoria
# * creek empieza a ser muy lento tras la fila 400, cada vez más hasta el 
# aburrimiento.  Aunque hay bastante memoria
# * saxlsx se pone lento entre las filas 460 y 700 y finalmente lo logra con
# este código:
#  Saxlsx::Workbook.open nomarc, auto_format: true do |w|
#    w.sheets.each do |s|
#      puts s.rows.count
#      nr = 0
#      s.rows.each do |r|
#        nr += 1
#        puts "#{nr} - #{r.inspect.length}"
#      end
#    end
#  end 
# * simple_xslx_reader: failed to allocate memory (NoMemoryError)
# * https://github.com/dmk/fastsheet requiere Rust
#
# El método más rápido que hemos encontrado es convertir de XLSX a CSV
# y después leer 
#
# Para convertir de XLSX a CSV es más rápido libreoffice que xlsx2csv

#`libreoffice --headless --convert-to csv --outdir ./conv/ BDLS_2019_limpio.xlsx`
#nomarc2 = "conv/#{nomarc.gsub('.xlsx', '.csv')}"


def normaliza(texto)
  r = texto.downcase.gsub('á', 'a')
  r.gsub!('Á', 'a')
  r.gsub!('é', 'e')
  r.gsub!('É', 'e')
  r.gsub!('í', 'i')
  r.gsub!('Í', 'i')
  r.gsub!('ó', 'o')
  r.gsub!('Ó', 'o')
  r.gsub!('ú', 'u')
  r.gsub!('Ú', 'u')
  r.gsub!('ü', 'u')
  r.gsub!('Ü', 'u')
  r.strip!
  r
end

divipola = CSV.read("divipola_sivel12.csv", headers: true)
STDERR.puts "Se leyeron #{divipola.count} centros poblados de Divipola"
divipola.each do |r|
  r['departamento'] = normaliza(r['departamento'])
  r['municipio'] = normaliza(r['municipio'])
  r['centropoblado'] = normaliza(r['centropoblado'])
end
puts "Divipola normalizado"

adiv = {}  # Arbol solo con nombres
adivcod = {} # Arbol con todo con llaves por código
bdep = {} # Departamento con llave por nombre
bmun = {} # Municipios con llave por código de departamento y nombre
divipola.each do |c|
  if !adiv[c['departamento']]
    bdep[c['departamento']] = c['cod_departamento'].to_i
    adiv[c['departamento']] = {}
    adiv[c['departamento']][c['municipio']] = {}
    bmun[c['cod_departamento'].to_i] = {}
  elsif !adiv[c['departamento']][c['municipio']]
    adiv[c['departamento']][c['municipio']] = {}
    bmun[c['cod_departamento'].to_i][c['municipio']] = c['cod_municipio'].to_i
  end
  adiv[c['departamento']][c['municipio']][c['centropoblado']] = 1
  if !adivcod[c['cod_departamento'].to_i]
    adivcod[c['cod_departamento'].to_i] = {
      departamento: c['departamento'],
      mun: {}
    }
  end
  if !adivcod[c['cod_departamento'].to_i][:mun][c['cod_municipio'].to_i]
    adivcod[c['cod_departamento'].to_i][:mun][c['cod_municipio'].to_i] = {
      municipio: c['municipio'],
      cp: {}
    }
  end
  adivcod[c['cod_departamento'].to_i][:mun][c['cod_municipio'].to_i][:cp][c['cod_centropoblado'].to_i] = c['centropoblado']
end

puts "Creado Divipola estilo árbol"

depalternos={
  11 => ['BOGOTÁ'],
  76 => ['VALLE'],
  88 => ['SAN ANDRÉS'],
}

# Versión iterativa de distancia Levenshtein por Set Schroeder
# https://stackoverflow.com/questions/8619785/what-is-an-efficient-way-to-measure-similarity-between-two-strings-levenshtein
def levenshtein(s1, s2)
  d = {}
  (0..s1.size).each do |row|
    d[[row, 0]] = row
  end
  (0..s2.size).each do |col|
    d[[0, col]] = col
  end
  (1..s1.size).each do |i|
    (1..s2.size).each do |j|
      cost = 0
      if (s1[i-1] != s2[j-1])
        cost = 1
      end
      d[[i, j]] = [d[[i - 1, j]] + 1,
                   d[[i, j - 1]] + 1,
                   d[[i - 1, j - 1]] + cost
      ].min
    end
  end
  return d[[s1.size, s2.size]]
end


def nombres_departamentos_equivalentes(cod_depto, nom, adivcod, depalternos, adv)
  if adivcod[cod_depto][:departamento] == normaliza(nom)
    return true
  end

  alterno = false
  if depalternos[cod_depto]
    depalternos[cod_depto].each do |an|
      if normaliza(nom) == normaliza(an)
        alterno = true
        adv << "Suponiendo departamento #{adivcod[cod_depto][:departamento].upcase}. "
      end
    end
  end
  return alterno
end

def nombres_municipios_equivalentes(cod_depto, cod_muni, nom, adivcod)
  nofi = adivcod[cod_depto][:mun][cod_muni][:municipio]
  nomn = normaliza(nom)
  return true if nofi == nomn
  if nofi.include?("(")
    # e.g BOLIVAR (CIUDAD BOLIVAR)
    nalt1 = nofi.split(/\(|\)/)[0].strip
    return true if nalt1 == nomn
    nalt2 = nofi.split(/\(|\)/)[1].strip
    return true if nalt2 == nomn
  end
  # e.g TUMACO y SAN ANDRÉS DE TUMACO
  return true if nom.length >= 4 && nofi.include?(nomn)
  return true if nofi.length >= 4 && nomn.include?(nofi)
  return true if 'la ' + nomn == nofi
  return true if 'la ' + nofi == nomn
  return true if nomn.gsub(/\s+/, "") == nofi.gsub(/\s+/, "")
  # e.g SAN ANDRES SOTAVENTA y SAN ANDRES DE SOTAVENTO
  return true if nomn.gsub(/ de /, " ") == nofi.gsub(/ de /, " ")
  # e.g LOPEZ (MICAY) y LOPEZ DE MICAY
  return true if nomn.gsub(/\(|\)/, "").gsub(/ de /, " ") == 
    nofi.gsub(/\(|\)/, "").gsub(/ de /, " ")
  if levenshtein(nomn, nofi) < 3
    return true
  end
  return false
end


# Función prestada --y que deben retroalimentar Sip::Helpers::FormatoFecha
def fecha_local_colombia_estandar(f, menserror=nil)
  # Date.strptime(f, '%d-%M-%Y') no ha funcionado,
  # %b debe ser en ingles
  # datepicker produce meses cortos comenzando en mayúsculas.
  # rails-i18n I18n.localize con %b produce mes en minuscula
  if !f
    return nil
  end
  if f == ''
    return ''
  end

  nf = nil
  pf = f.split('/')
  if pf.count < 3
    pf = f.split('-')
  end
  if pf.count < 3
    if menserror
      menserror << "  Formato de fecha en locale de colombia desconocido: #{f}"
    else
      puts "Formato de fecha en locale de colombia desconocido: #{f}"
    end
    return nil;
    nf = Date.strptime(f, '%d-%M-%Y').strftime('%Y-%m-%d')
  else
    return nil if !pf[1]
    m = case pf[1].downcase
        when 'ene', 'ene.', 'jan', 'jan.', '1', '01'
          1
        when 'feb', 'feb.', '2', '02'
          2
        when 'mar', 'mar.', '3', '03'
          3
        when 'abr', 'abr.', 'apr', 'apr.', '4', '04'
          4
        when 'may', 'may.', '5', '05'
          5
        when 'jun', 'jun.', '6', '06'
          6
        when 'jul', 'jul.', '7', '07'
          7
        when 'ago', 'ago.', 'aug', 'aug.', '8', '08'
          8
        when 'sep', 'sep.', '9', '09'
          9
        when 'oct', 'oct.', '10'
          10
        when 'nov', 'nov.', '11'
          11
        when 'dic', 'dic.', 'dec', 'dec.', '12'
          12
        else
          menserror << "  Formato de fecha en locale de Colombia con mes desconocido suponiendo 12."
          12
        end
    a = pf[2].to_i
    if (a < 100)
      a += 2000
    end
    begin
      nf = Date.new(a, m, pf[0].to_i).strftime('%Y-%m-%d')
    rescue
      if menserror
        menserror << "  Formato de fecha en locale de colombia desconocido: #{f}"
      else
        puts "Formato de fecha en locale de colombia desconocido: #{f}"
      end
    end
  end
  return nf
end

# Función prestada --y que deben retroalimentar Sip::Helpers::FormatoFecha
# Convierte una fecha de formato local a formato estándar
def fecha_local_estandar f
  if !f
    return nil
  end
  if f == ''
    return ''
  end
  case Rails.application.config.x.formato_fecha
  when 'dd/M/yyyy', 'dd-M-yyyy'
    nf = fecha_local_colombia_estandar f
  when 'dd-mm-yyyy'
    nf = Date.strptime(f, '%d-%m-%Y').strftime('%Y-%m-%d')
  when 'dd/mm/yyyy'
    nf = Date.strptime(f, '%d/%m/%Y').strftime('%Y-%m-%d')
  else
    nf = Date.strptime(f, '%Y-%m-%d').strftime('%Y-%m-%d')
  end
  return nf
end

# Función prestada --y que deben retroalimentar Sip::Helpers::FormatoFecha
def reconoce_adivinando_locale(f, menserror = nil)
  if !f || (f.class != String && f.class != Date) ||
      (f.class == String && f == '')
    return nil
  end
  if f.class == Date
    return f
  end
  nf = f  # nf se espera yyyy-mm-dd
  if f.include?('/')
    #'dd/M/yyyy'
    nf =fecha_local_colombia_estandar(f, menserror)
  else
    if f.include?('-')
      p = f.split('-')
      if p[0].to_i >= 1 && p[0].to_i <=31 && p[2].to_i >= 0
        nf = fecha_local_colombia_estandar(f, menserror)
      end
    end
  end
  begin
    r = Date.strptime(nf, '%Y-%m-%d')
  rescue
    r = nil
    if menserror
      menserror << "  Formato de fecha desconocido: #{f}"
    else
      puts "Formato de fecha desconocido: #{f}"
    end
  end

  return r
end


coberturas = ['S', 'N', 'M', 'SR', 'I', 'D', 'R']

convocantes = [100, 120, 130, 140, 170, 180,
               200, 210, 250, 280, 290,
               310, 320, 350, 360, 380, 390,
               400, 450, 480, 
               500, 570, 580, 585, 590,
               600,
               700, 750,
               800, 805, 810, 815, 820, 830, 840, 850, 860, 870, 875, 880, 885, 890,
               900, 940, 950, 980
]

actores = [100, 110, 120, 130, 140, 150, 160, 161, 162,
           200, 
           300, 310, 320, 330, 350,
           400, 410, 420,
           500, 510, 520, 530,
           600,
           700,
           750,
           800,
           900
]


tipos_lucha = {
  'P' => 1,
  'PC' => 1,
  'PA' => 1,
  'PE' => 1,
  'PT' => 1,
  'B' => 2,
  'M' => 3,
  'I' => 4,
  'D' => 5,
  'T' => 6,
  'RC' => 7,
  'HH' => 8
}

motivos = [
  110, 111, 112, 113, 114, 115, 120, 130, 140,
 1000, 1500,
 2100, 2200, 2400, 2500,
 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3810, 3820, 3830, 3840, 3850, 3860, 3870,
 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700,
 5000, 5100, 5200, 5300, 5500,
 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6900,
 7000, 7100, 7105, 7200, 7205, 7300, 7305, 7400, 7405, 7500, 7505, 7900,
 8000, 8100, 8110, 8120, 8130, 8135, 8140, 8150, 8160, 8170, 8180, 8190,
 8200, 8300, 8310, 8350, 8360, 8400, 8500, 8600, 8900,
 9000, 9500,
]

adversarios = [
  1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,
  2000, 2100, 2200, 2300,
  3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700,
  4000, 4100, 4200, 4300,
  5000, 5100,
  6000, 6100, 6200, 6300, 6500,
  7000, 7100, 7200, 7300, 7400, 7500,
  8000, 8100, 8500, 8600,
  9000, 9100, 9200, 9300,
  130, 140, 150
]

csv = CSV.read(nomarc, headers: true, encoding: 'utf-8', 
               unconverted_fields: true)

puts "Leyendo #{csv.count} filas del archivo #{nomarc}"
procesa = true
enc_obligatorios = 
  { fecha: ['FECHA'],
    anio: ['AÑO'],
    cobertura: ['COBERTURA-AMBITO', 'C'],
    registro: ['REGISTRO'],
    depto: ['DEPTO'],
    cod_depto: ['COD DEPTO', 'DEPTO1'],
    nmuni: ['NMUNI'],
    codigo_convocantes: ['CODIGO CONVOCANTES', 'CONVOCAN'],
    dirig1: ['DIRIG1'],
    dirig2: ['DIRIG2', nil],
    dirig3: ['DIRIG3'],
    actor: ['ACTOR'],
    partici1: ['PARTICI1', 'PARTICIP'],
    partici2: ['PARTICI2'],
    partici3: ['PARTICI3'],
    partici4: ['PARTICI4'],
    tipo_lucha: ['TIPO LUCHA','TL'],
    accion: ['ACCION'],
    motivopl: ['MOTIVOPL'],
    motivopp: ['MOTIVOPP'],
    motivo2: ['MOTIVO2'],
    motivo3: ['MOTIVO3'],
    motivo4: ['MOTIVO4'],
    motivo5: ['MOTIVO5'],
    adversario: ['ADVERSARIO', 'ADVERSA'],
    entidad1: ['ENTIDAD1', '. '],
    entidad2: ['ENTIDAD2'],
    entidad3: ['ENTIDAD3'],
    fuente: ['FUENTE'],
    ffuente: ['FFUENTE', ', '],
    ffuen_1: ['FFUEN_1'],
    memo: ['MEMO']
}


(1..32).each do |nm|
  muni="muni#{nm}".to_s
  mpio="mpio#{nm}".to_s
  enc_obligatorios[muni.to_sym]=["MUNI#{nm}"]
  enc_obligatorios[mpio.to_sym]=["MPIO#{nm}"]
end


enc = {}
open(nomerr, 'w') do |f|
  enc_obligatorios.each do |l, p|
    encontro = false
    ip = 0
    while !encontro && ip<p.length
      if csv.headers.include?(p[ip])
        encontro = true
        enc[l] = p[ip]
      end
      ip += 1
    end
    if !encontro
      f << "Falta columna indispensable con uno de estos nombres:  '#{p}'\n"
      procesa = false
    end
  end
end
if !procesa
  puts "No se procesa por errores"
  byebug
  exit 1
end
if csv.headers.include?('ORGANIZACIONES CONVOCANTES')
  enc[:orgconvocan]='ORGANIZACIONES CONVOCANES'
end


csal = 0
numls = 0
sinprob = 0
ultregistro = 0
ultfecha = nil
ultanio = nil

CSV.open(nomerr, "wb") do |csverr|
  csverr << (csv.headers + ['PROBLEMAS'])
  CSV.open(nomadv, "wb") do |csvadv|
    csvadv << (csv.headers + ['ADVERTENCIAS'])
    (0..csv.count-1).each do |cfila|
      prob = ''
      adv = ''
      mes_inexacto = false
      anio = csv[cfila][enc[:anio]].to_i
      if anio <= 1900 || (ultanio != nil && anio != ultanio && anio != ultanio + 1)
        prob << "Año #{anio} no válido. "
      end

      if csv[cfila][enc[:fecha]].nil? || csv[cfila][enc[:fecha]].strip.empty?
        if anio <= 1900
          prob << 'Falta FECHA. '
        else
          # Autocompleta fecha conn 15/Jun del año y mes_inexacto es verdadero
          fecha = Date.new(anio, 6, 15)
          mes_inexacto = true
          adv << "Suponiendo que la fecha es 15/Jun/#{anio} con mes inexacto"
        end
      else
        me = ''
        fecha = reconoce_adivinando_locale(csv[cfila][enc[:fecha]], me)
        if me != ''
          prob << me + " #{csv[cfila][enc[:fecha]]}"
        end
        if fecha && anio != fecha.year
          if fecha.year - 100 == anio
            # Cambiar de siglo
            fecha = Date.new(anio, fecha.month, fecha.day)
          else
            prob << "Campo año (#{anio}) no corresponde al " +
              " año del campo fecha (#{fecha.year}). "
          end
        end
      end

      if !csv[cfila][enc[:registro]]
        prob << "Se esperaba valor para REGISTRO. "
      end
      registro = csv[cfila][enc[:registro]].to_i
      if cfila == 0 && registro != 1
        prob << "Primera fila debía tener REGISTRO en 1. "
      elsif cfila > 0 && registro != 1 
        if ultregistro + 1 != registro
          prob << "Se esperaba REGISTRO en valor #{ultregistro+1}. "
        end
      end

      if registro == 1
        if !csv[cfila][enc[:cobertura]]  || csv[cfila][enc[:cobertura]].strip.empty?
          prob << "Se esperaba algún valor en #{enc[:cobertura]}"
        elsif !coberturas.include?(csv[cfila][enc[:cobertura]].strip.upcase)
          prob << "#{enc[:cobertura]} desconocido: #{csv[cfila][enc[:cobertura]]}. "
        else
          cobertura = csv[cfila][enc[:cobertura]].strip.upcase
        end
      elsif csv[cfila][enc[:cobertura]]  && !csv[cfila][enc[:cobertura]].strip.empty?
        adv << "#{enc[:cobertura]} se esperaba vacío. "
      end

      cod_depto = nil
      if !csv[cfila][enc[:cod_depto]]
        if csv[cfila][enc[:depto]] != 'NACIONAL'
          if !csv[cfila][enc[:depto]]
            prob << "Se esperaba valor en campo #{enc[:cod_depto]}. "
          else
            adv << "Se esperaba valor en campo #{enc[:cod_depto]}. "
            depton = normaliza(csv[cfila][enc[:depto]])
            cod_depto = bdep[depton]
            if cod_depto && cod_depto.to_i > 0
              adv << "Empleando #{cod_depto} en campo vacío #{enc[:cod_depto]}. "
            else
              adivcod.each do |cd, r|
                if nombres_departamentos_equivalentes(cd, csv[cfila][enc[:depto]], adivcod, depalternos, adv)
                  cod_depto = cd
                  break
                end
              end
              if cod_depto && cod_depto >0
                adv << "Empleando #{cod_depto} en campo vacío #{enc[:cod_depto]}. "
              else
                prob << "No logra reconocerse departamento #{csv[cfila][enc[:depto]]}"
              end
            end
          end
        end
      elsif csv[cfila][enc[:depto]] == 'NACIONAL'
        # Ignorar cod_depto y nmuni
        cod_depto = nil
      else
        cod_depto = csv[cfila][enc[:cod_depto]].to_i
        if !adivcod[cod_depto]
          prob << "DIVIPOLA no tiene código de departamento #{cod_depto}. "
        else
          if !csv[cfila][enc[:depto]]
            prob << "Se esperaba valor en campo #{enc[:depto]}. "
          else
            if !nombres_departamentos_equivalentes(cod_depto, csv[cfila][enc[:depto]], adivcod, depalternos, adv)
              prob << "En campo '#{enc[:depto]}' se esperaba algo como #{adivcod[cod_depto][:departamento].upcase} pero se encontró #{csv[cfila][enc[:depto]]}. "
            end
          end
        end
      end

      if cod_depto && cod_depto > 1
        # Analizar municipios que sean del departamento
        mllenos = 0
        (1..32).each do |nm|
          muni="muni#{nm}".to_s
          mpio="mpio#{nm}".to_s
          #enc[muni.to_sym]
          #enc[mpio.to_sym]
          if (csv[cfila][enc[muni.to_sym]].nil? || 
              csv[cfila][enc[muni.to_sym]].strip.empty?) &&
            !csv[cfila][enc[mpio.to_sym]].nil? &&
            !csv[cfila][enc[mpio.to_sym]].strip.empty? 
            mllenos += 1
            cod_muni = bmun[cod_depto][normaliza(csv[cfila][enc[mpio.to_sym]].strip)]
            if cod_muni
              cod_muni = cod_muni.to_i
              adv << "Usando #{cod_muni} en campo vacío #{enc[muni.to_sym]}. "
            else
              cod_muni = nil
              adivcod[cod_depto][:mun].each do |cm, r|
                if nombres_municipios_equivalentes(cod_depto, cm, 
                    csv[cfila][enc[mpio.to_sym]], adivcod)
                  cod_muni = cm
                  break
                end
              end
              if cod_muni
                adv <<  "Campo vacío #{enc[muni.to_sym]}, suponiendo que se trata del municipio #{adivcod[cod_depto][:mun][cod_muni][:municipio]} con código #{cod_muni}. "
              else
                prob << "Campo vacío #{enc[muni.to_sym]} y no puede identificarse municipio #{csv[cfila][enc[mpio.to_sym]]} en departamento con código #{cod_depto}. "
              end
            end
          elsif !csv[cfila][enc[muni.to_sym]].nil? &&
            !csv[cfila][enc[muni.to_sym]].strip.empty? &&
            (csv[cfila][enc[mpio.to_sym]].nil? || csv[cfila][enc[mpio.to_sym]].strip.empty?)
            if csv[cfila][enc[muni.to_sym]].to_i > 0 && 
                adivcod[cod_depto][:mun][csv[cfila][enc[muni.to_sym]].to_i]
              cod_muni = csv[cfila][enc[muni.to_sym]].to_i
              adv << "Suponiendo que campo vació #{enc[mpio.to_sym]} es '#{adivcod[cod_depto][:mun][cod_muni][:municipio].upcase}'. "
            else
              cod_muni = nil
              adivcod[cod_depto][:mun].each do |cm, r|
                if nombres_municipios_equivalentes(cod_depto, cm, 
                    csv[cfila][enc[muni.to_sym]], adivcod)
                  cod_muni = cm
                  break
                end
              end
              if cod_muni
                adv <<  "Campo vacío #{enc[mpio.to_sym]}, suponiendo que es #{cod_muni}. "
              else
                prob << "Campo vacío #{enc[mpio.to_sym]} y no pudo identificarse municipio del campo #{enc[muni.to_sym]}. "
              end
            end
            mllenos += 1
          elsif (!csv[cfila][enc[muni.to_sym]].nil? &&
                 !csv[cfila][enc[mpio.to_sym]].nil?) 
            mllenos += 1
            # Que concuerden código y nombre
            cod_muni = csv[cfila][enc[muni.to_sym]].to_i
            if cod_muni >= 0 && cod_depto > 0 && 
                !adivcod[cod_depto][:mun][cod_muni]
              prob << "DIVIPOLA no tiene código de municipio #{cod_muni}. "
            elsif !nombres_municipios_equivalentes(cod_depto, cod_muni, csv[cfila][enc[mpio.to_sym]], adivcod)
              prob << "En campo #{enc[mpio.to_sym]} se esperaba algo como '#{adivcod[cod_depto][:mun][cod_muni][:municipio].upcase}' pero se encontró '#{csv[cfila][enc[mpio.to_sym]]}'. "
            else
              # Bien este municipio
              if normaliza(csv[cfila][enc[mpio.to_sym]]) != adivcod[cod_depto][:mun][cod_muni][:municipio]
                adv << "Suponiendo que el municipio es #{adivcod[cod_depto][:mun][cod_muni][:municipio].upcase}"
              end
            end
          end
        end

        nmuni = csv[cfila][enc[:nmuni]].to_i # Ignorarlo
        if mllenos != nmuni
          adv << "Se esperaba que NMUNI fuera #{mllenos} pero es #{nmuni}. "
        end
      end


      # Convocantes
      if registro == 1
        if csv[cfila][enc[:codigo_convocantes]].nil?
          prob << "Falta valor en campo #{enc[:codigo_convocantes]} en registro 1. "
        elsif !convocantes.include?(csv[cfila][enc[:codigo_convocantes]].to_i)
          prob << "Valor #{csv[cfila][enc[:codigo_convocantes]].to_i} no es valido en campo #{enc[:codigo_convocantes]}. "
        else
          codigo_convocantes = csv[cfila][enc[:codigo_convocantes]].to_i
        end
      end

      if csv[cfila][enc[:dirig1]] && csv[cfila][enc[:dirig1]].length > 512
        prob << "Valor en campo #{enc[:dirig1]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:dirig2]] && csv[cfila][enc[:dirig2]].length > 512
        prob << "Valor en campo #{enc[:dirig2]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:dirig3]] && csv[cfila][enc[:dirig3]].length > 512
        prob << "Valor en campo #{enc[:dirig3]} de más de 512 caracteres. "
      end

      # Actor
      actor = nil
      if registro == 1
        if csv[cfila][enc[:actor]].nil?
          prob << "Falta valor en campo #{enc[:actor]} en registro 1. "
        elsif !actores.include?(csv[cfila][enc[:actor]].to_i)
          prob << "Valor #{csv[cfila][enc[:actor]].to_i} no es valido en campo #{enc[:actor]}. "
        else
          actor = csv[cfila][enc[:actor]].to_i
        end
      end

      if csv[cfila][enc[:partici1]] && csv[cfila][enc[:partici1]].length > 512
        prob << "Valor en campo #{enc[:partici1]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:partici2]] && csv[cfila][enc[:partici2]].length > 512
        prob << "Valor en campo #{enc[:partici2]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:partici3]] && csv[cfila][enc[:partici3]].length > 512
        prob << "Valor en campo #{enc[:partici3]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:partici4]] && csv[cfila][enc[:partici4]].length > 512
        prob << "Valor en campo #{enc[:partici4]} de más de 512 caracteres. "
      end

      # Tipo de Lucha
      tipo_lucha = nil
      if registro == 1
        if csv[cfila][enc[:tipo_lucha]].nil?
          prob << "Falta valor en campo #{enc[:tipo_lucha]} en registro 1. "
        elsif !tipos_lucha.keys.include?(csv[cfila][enc[:tipo_lucha]])
          prob << "Valor #{csv[cfila][enc[:tipo_lucha]]} no es valido en campo #{enc[:tipo_lucha]}. "
        else
          tipo_lucha = csv[cfila][enc[:tipo_lucha]]
        end
      end

      # Acción
      accion = nil
      if registro == 1
        if csv[cfila][enc[:accion]].nil?
          prob << "Falta valor en campo #{enc[:accion]} en registro 1. "
        elsif !tipos_lucha.values.uniq.include?(csv[cfila][enc[:accion]].to_i)
          prob << "Valor #{csv[cfila][enc[:accion]]} no es valido en campo #{enc[:accion]}. "
        elsif tipo_lucha && tipos_lucha[tipo_lucha] != csv[cfila][enc[:accion]].to_i
          prob << "En campo #{enc[:accion]} se esperaba el valor #{tipos_lucha[tipo_lucha]}, pero aparece #{csv[cfila][enc[:accion]].to_i}. "
        else
          accion = csv[cfila][enc[:accion]].to_i
        end
      end

      # Motivo PL
      motivopl = nil
      if registro == 1
        if csv[cfila][enc[:motivopl]].nil?
          prob << "Falta valor en campo #{enc[:motivopl]} en registro 1. "
        elsif !motivos.include?(csv[cfila][enc[:motivopl]].to_i)
          prob << "Valor #{csv[cfila][enc[:motivopl]].to_i} no es valido en campo #{enc[:motivopl]}. "
        else
          motivopl = csv[cfila][enc[:motivopl]].to_i
        end
      end

      if csv[cfila][enc[:motivopp]] && csv[cfila][enc[:motivopp]].length > 512
        prob << "Valor en campo #{enc[:motivopp]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:motivo2]] && csv[cfila][enc[:motivo2]].length > 512
        prob << "Valor en campo #{enc[:motivo2]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:motivo3]] && csv[cfila][enc[:motivo3]].length > 512
        prob << "Valor en campo #{enc[:motivo3]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:motivo4]] && csv[cfila][enc[:motivo4]].length > 512
        prob << "Valor en campo #{enc[:motivo4]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:motivo5]] && csv[cfila][enc[:motivo5]].length > 512
        prob << "Valor en campo #{enc[:motivo5]} de más de 512 caracteres. "
      end

      # Adversario
      adversario = nil
      if registro == 1
        if csv[cfila][enc[:adversario]].nil?
          prob << "Falta valor en campo #{enc[:adversario]} en registro 1. "
        elsif !adversarios.include?(csv[cfila][enc[:adversario]].to_i)
          prob << "Valor #{csv[cfila][enc[:adversario]].to_i} no es valido en campo #{enc[:adversario]}. "
        else
          adversario = csv[cfila][enc[:adversario]].to_i
        end
      end

      if csv[cfila][enc[:entidad1]] && csv[cfila][enc[:entidad1]].length > 512
        prob << "Valor en campo #{enc[:entidad1]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:entidad2]] && csv[cfila][enc[:entidad2]].length > 512
        prob << "Valor en campo #{enc[:entidad2]} de más de 512 caracteres. "
      end
      if csv[cfila][enc[:entidad3]] && csv[cfila][enc[:entidad3]].length > 512
        prob << "Valor en campo #{enc[:entidad3]} de más de 512 caracteres. "
      end

      # Fuente
      if registro == 1 && (!csv[cfila][enc[:fuente]] || csv[cfila][enc[:fecha]].strip.empty?)
        prob << "Falta #{enc[:fuente]}. "
      elsif csv[cfila][enc[:fuente]] && csv[cfila][enc[:fuente]].length > 512
        prob << "Valor en campo #{enc[:fuente]} de más de 512 caracteres. "
      end

      fuente = nil
      if csv[cfila][enc[:fuente]] && !csv[cfila][enc[:fuente]].strip.empty?
        fuente = csv[cfila][enc[:fuente]].strip
      end

      # Fecha fuente
      if registro == 1 && (!csv[cfila][enc[:ffuente]] || csv[cfila][enc[:ffuente]].strip.empty?)
        prob << "Falta #{enc[:ffuente]}. "
      end
      if csv[cfila][enc[:ffuente]] && !csv[cfila][enc[:ffuente]].strip.empty?
        me = ''
        ffuente = reconoce_adivinando_locale(csv[cfila][enc[:ffuente]], me)
        if me != ''
          prob << "#{enc[:ffuente]}: #{csv[cfila][enc[:ffuente]]}: #{me}. "
        else
          if ffuente && anio != ffuente.year
            if ffuente.year - 100 == anio
              # Cambiar de siglo
              ffuente= Date.new(anio, ffuente.month, ffuente.day)
            end
          end
          if ffuente < fecha
            adv << "#{enc[:ffuente]} (#{csv[cfila][enc[:ffuente]]}) es " +
              "anterior a #{enc[:fecha]} (#{csv[cfila][enc[:fecha]]}). "
          end
        end
      end

      if fuente && !ffuente
        prob << "Hay  #{enc[:fuente]} pero no #{enc[:ffuente]}. "
      elsif !fuente && ffuente
        prob << "Hay  #{enc[:ffuente]} pero no #{enc[:fuente]}. "
      end


      if csv[cfila][enc[:ffuen_1]] && !csv[cfila][enc[:ffuen_1]].strip.empty?
        me = ''
        ffuen_1 = reconoce_adivinando_locale(csv[cfila][enc[:ffuen_1]], me)
        if me != ''
          prob << "#{enc[:ffuen_1]}: #{csv[cfila][enc[:ffuen_1]]}: #{me}. "
        else
          if ffuen_1 && anio != ffuen_1.year
            if ffuen_1.year - 100 == anio
              # Cambiar de siglo
              ffuen_1= Date.new(anio, ffuen_1.month, ffuen_1.day)
            end
          end
          if ffuen_1 < fecha
            adv << "#{enc[:ffuen_1]} (#{csv[cfila][enc[:ffuen_1]]}) es " +
              "anterior a #{enc[:fecha]} (#{csv[cfila][enc[:fecha]]}). "
          end
        end
      end

      if !fuente && ffuen_1
        prob << "Hay  #{enc[:ffuen_1]} pero no #{enc[:fuente]}. "
      end


      if registro == 1 && csv[cfila][enc[:memo]].nil?
        prob << 'Falta MEMO en registro 1. '
      end


      ultregistro = registro

      if adv != ''
        csvadv << csv[cfila].to_h.values + ["Fila #{cfila+2}: #{adv}"]
      end

      if prob != ''
        puts "* #{cfila+2}:  #{prob}"
        csverr << csv[cfila].to_h.values + ["La fila #{cfila+2} tiene estos problemas: #{prob}"]
      else
        sinprob += 1
      end

      #  elsif importa
      #    nls = ::Ls.create(fecha: csv[cfila][enc[:fecha]],
      #                      convocante_id: csv[cfila][enc[:convocante]],
      #                      memo: csv[cfila][enc[:memo]])

    end

  end #csvadv
end #csverr


p1 = (csv.count-sinprob)*1000/csv.count
p2 = (sinprob)*1000/csv.count
puts "De los #{csv.count} registros, tienen problema #{csv.count-sinprob} (#{p1.round/10.0}%) y no tienen problema #{sinprob} (#{p2.round/10.0}%)"
